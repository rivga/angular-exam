import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection = this.db.collection('students');
  
  getStudents():Observable<any[]>{
    this.studentsCollection = this.db.collection(`students`,
    ref => ref.orderBy('timestamp','desc'));
    return this.studentsCollection.snapshotChanges()
  }
  
  

  //updateRsult(userId:string, id:string,result:string){

    //updateStudent(userId:string, id:string,gradeaverage:number,gradepsy:number,paid:boolean){
     // this.db.doc(`users/${userId}/students/${id}`).update(
      //  {
        //  gradeaverage:gradeaverage,
         //: gradepsy:gradepsy,
         // paid:paid,
          //result:null
        //}
      //)
    //}

     public deleteStudent( id:string){
     this.db.doc(`students/${id}`).delete();
    }


    addStudent(name:string,gradeaverage:number,gradepsy:number,paid:boolean){
      const student = {name:name, gradeaverage:gradeaverage,gradepsy:gradepsy, paid:paid}
      this.studentsCollection.add(student);
    } 

  constructor(private db: AngularFirestore,) { }
}
