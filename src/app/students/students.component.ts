import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';
import { PredictionService } from './../prediction.service';



@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;

  students:Student[];
  students$;
  editstate = [];
  addStudentFormOpen = false;
  panelOpenState = false;
  rowToSave:number = -1; 
  //addCustomerFormOpen

  deleteStudent(id:string){
    this.studentsService.deleteStudent(id);
  }
  

  

  //: string[] = ['gradeaverage', 'gradepsy ', 'paid'];
 


  predict(i){
    this.rowToSave = i; 
    console.log(this.students[i]);
    this.predictionService.predict(this.students[i].gradeaverage, this.students[i].gradepsy, this.students[i].paid).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'will not go out';
        } else {
          var result = 'will go out'
        }
        this.students[i].result = result;
        console.log(result);
      
      }
    );   
      }

    cancel(i){
      this.rowToSave = null; 
      this.students[i].result = null;
    }



  constructor(public authService:AuthService, private studentsService:StudentsService, private predictionService: PredictionService) { }

    ngOnInit(): void {
      this.students$ = this.studentsService.getStudents();
      this.students$.subscribe(
        docs =>{
          this.students = [];
          for(let document of docs){
            const student:Student = document.payload.doc.data();
            if(student.result){
              student.saved = true; 
            }
            student.id = document.payload.doc.id;
            this.students.push(student);
          }
        }
      )
    }
}