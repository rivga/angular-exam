import { Student } from './../interfaces/student';
import { StudentsService } from '../students.service';
import { PredictionService } from './../prediction.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';





@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() name:string;
  @Input() gradeaverage:number;
  @Input() gradepsy:number;
  @Input() paid:boolean;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>()
  @Output() closeEdit = new EventEmitter<null>()
  isErroraver:boolean = false;
  isErrorpsy:boolean = false;

  result:string;
  rowToSave:boolean = false; 
  email:string;
  timestamp:number =Date.now();

  tellParentToClose(){
    this.closeEdit.emit();
  }

  updateParent(){
    let student:Student = {id:this.id, name:this.name, gradeaverage:this.gradeaverage, gradepsy:this.gradepsy, paid:this.paid};
    if(this.gradepsy < 0 || this.gradepsy > 800){
      this.isErrorpsy = true;
    }
    else if(this.gradeaverage < 0 || this.gradeaverage > 100){
      this.isErroraver = true;
    }
    else{
    this.update.emit(student);
    if(this.formType == "Add Student"){
      this.name = null;
      this.gradeaverage = null;
      this.gradepsy = null;
      this.paid = false;
    }
   }
  }

  add(){
    this.studentsService.addStudent(this.name, this.gradeaverage, this.gradepsy, this.paid, );
    this.router.navigate(['/students']);
  }

  predict(){
    this.isErroraver = false;
    this.isErrorpsy = false;
    if(this.gradepsy < 0 || this.gradepsy > 800){
      this.isErrorpsy = true;
    }
    else if(this.gradeaverage < 0 || this.gradeaverage > 100){
      this.isErroraver = true;
    }
    else{
    this.predictionService.predict(this.gradeaverage, this.gradepsy, this.paid).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'will no go out';
        } else {
          var result = 'will go out'
        }
        this.result = result;
        console.log(result);
        this.rowToSave = true;
      }
    );   
    }
      }

      cancel(){
        this.rowToSave = false;
        this.name = null;
        this.gradeaverage = null;
        this.gradepsy = null;
        this.paid = null;
        this.result = null;
      }

      


  constructor(public authService:AuthService, private studentsService:StudentsService, private predictionService:PredictionService, private router:Router) { }

  ngOnInit(): void {
  }

}
