export interface Student {
    id:string,
    name:string,
    gradeaverage: number;
    gradepsy: number;
    paid: boolean;
    result?:string,
    saved?:boolean ,
    timestamp?:number;

}

